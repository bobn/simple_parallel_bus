#
# Copyright 2022 - 2023 Robert Newgard
#
# This file is part of the simple_parallel_bus project
#
# simple_parallel_bus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# simple_parallel_bus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
#

ifneq ($(INCLUDED_VERILATOR),)
    $(warning verilator included earlier)
endif

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_HINTS),)
    include bin/include-hints.mk
endif

ifeq ($(INCLUDED_SEPARATOR),)
    include bin/include-separator.mk
endif

ifeq ($(INCLUDED_CONFIG),)
    include bin/include-config.mk
endif

ifeq ($(CFG_VLTR),)
    CFG_VLTR := $(CFG)/Install/Verilator
endif

ifeq ($(GENHTML),)
    GENHTML := $(call exe_chk, genhtml)
endif



# -- Integrating Verilator -----------------------------------------------------
#
# 1. Configuration files that must exist (empty okay)
#
#     | File                                         | Description
#     |----------------------------------------------|--------------------------------
#     | cfg/Testbench/active_testbench               | Active testbench
#     | cfg/Testbench/$BENCH_NAME/active_test        | Active test name
#     | cfg/Testbench/$BENCH_NAME/cover_lines        | Enable line coverage
#     | cfg/Testbench/$BENCH_NAME/cover_toggles      | Enable toggle coverage
#     | cfg/Testbench/$BENCH_NAME/lib_hdr            | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/lib_obj            | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/link_libs          | See include file for SystemC
#     | cfg/Testbench/$BENCH_NAME/tb_csrc            | Testbench C++ code
#     | cfg/Testbench/$BENCH_NAME/tb_hsrc            | Testbench C++ headers
#     | cfg/Testbench/$BENCH_NAME/tb_vinc            | Testbench SystemVerilog include dirs (-y)
#     | cfg/Testbench/$BENCH_NAME/tb_vsrc            | Testbench SystemVerilog sources
#     | cfg/Testbench/$BENCH_NAME/tb_vsrc_top        | SystemVerilog top module
#
#
# 2. Configuration files that may exist
#
#     | File                                           | Description
#     |------------------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/Defines/PlusArgs/*   | File names used as plusarg keys, containing value strings
#     | cfg/Testbench/$BENCH_NAME/Defines/Parameters/* | File names used as plusarg and parameter keys, containing value strings
#
#     Warning
#         1. As currently implemented, the make-env target cannot pass
#            <size>'<base><value> format arguments to Verilator
#
#     Notes
#         1. Parameters are used by Verilator; they are passed into Verilator
#            using "-pvalue+<name>=<value>" arguments
#         2. PlusArgs and Parameters are passed into the SystemC testbench using
#            "+<name>=<value>" arguments; these values are accessed in SystemC
#            by calling the Verilated::commandArgsPlusMatch() function, and
#            accessed in the Verilated model using the $value$plusargs() task
#         3. See SystemC include file for PlusArgs implementation
#
#
# 3. Variables that must be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SIM_WRK_DIR        | 
#     | SIM_CFG_DIR        | 
#
#
# 4. Variables that may be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | CFG_VLTR           | 
#
#
# 5. Add hints to Makefile
#     * Add the following to the '$(eval $(call hints_targ, ...' sequence
#
#         $(vltr_lib_hints)
#         $(vltr_cov_hints)
#
#
# 6. Add rules to Makefile
#     * Add the following to the rules
#
#         $(vltr_lib_rules)
#         $(vltr_cov_rules)
#
#--


VLTR_VERS_CFG    := $(CFG_VLTR)/version
VLTR_VERS_DFLT   := v4.224
VLTR_VERS_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_VERS_CFG),$(VLTR_VERS_DFLT))))
VLTR_HOME_CFG    := $(CFG_VLTR)/home
VLTR_HOME_DFLT   := /opt/verilator
VLTR_HOME_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_HOME_CFG),$(VLTR_HOME_DFLT))))

VLTR_BIN_PATH    := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/bin
VLTR_INC         := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/share/verilator/include
VLTR_INC_STD     := $(VLTR_HOME_SEL)/$(VLTR_VERS_SEL)/share/verilator/include/vltstd
VLTR_CFLAGS      := $(if $(FALSE),$(foreach FLAG,$(CFLAGS),-CFLAGS $(FLAG)))
VLTR_VOPT        := $(VLTR_CFLAGS) --sc --trace
VLTR_OBJS        := verilated.o verilated_vcd_c.o verilated_vcd_sc.o

$(eval $(call make_pkg_export_prepath, PATH, $(VLTR_BIN_PATH)))

VERILATOR := $(call exe_chk, verilator)


define vltr_chk_src_cfg_recipe
    $(if $(VLTR_SRC_CFG),$(NULL),$(error Verilator source config file path variable VLTR_SRC_CFG is empty))
    $(if $(wildcard $(VLTR_SRC_CFG)),$(NULL),$(error Verilator source config path $(VLTR_SRC_CFG) does not exist))
    @echo "[INF] Verilator source config okay."
endef

define vltr_chk_src_list_recipe
    $(if $(VLTR_SRC_LST),$(NULL),$(error No verilator sources in VLTR_SRC_LST))
    @echo "[INF] Verilator source list okay."
endef

define vltr_chk_top_name_cfg_recipe
    $(if $(VLTR_TOP_CFG),$(NULL),$(error Verilator top module name config file variable VLTR_TOP_CFG is empty))
    $(if $(wildcard $(VLTR_TOP_CFG)),$(NULL),$(error Verilator top module name config file $(VLTR_TOP_CFG) does not exist))
    @echo "[INF] Top RTL module name config okay."
endef

define vltr_chk_top_name_recipe
    $(if $(VLTR_TOP_SEL),$(NULL),$(error Verilator top module name variable VLTR_TOP_SEL is empty))
    @echo "[INF] Top RTL module name is $(VLTR_TOP_SEL)."
endef

define vltr_chk_target_path_recipe
    $(if $(VLTR_TARG),$(NULL),$(error Verilator target archive name variable VLTR_TARG is empty))
    @echo "[INF] Target archive name is $(VLTR_TARG)."
endef

define vltr_lib_create_dir_recipe
    $(if $(VLTR_WRK_DIR),$(NULL),$(error Verilator working directory path variable VLTR_WRK_DIR is empty))
    @echo "[INF] (Re)creating Verilator working directory $(VLTR_WRK_DIR)..."
    rm -rf $(VLTR_WRK_DIR)
    mkdir -p $(VLTR_WRK_DIR)
    @echo "[INF] Verilator working directory created."
endef

define vltr_lib_verilate_recipe
    @echo "[INF] Verilating..."
    $(VERILATOR) $(VLTR_VOPT) --Mdir $(VLTR_WRK_DIR) --top-module $(VLTR_TOP_SEL) $(VLTR_SRC_LST)
    @echo "[INF] Done verilating."
endef

# vltr_lib_make_check_recipe <fil>
define vltr_lib_make_check_recipe
    @ echo "[INF] Checking $(strip $(1))..."
    @ test -f $(strip $(1)) || (echo "[ERR] Failed to create $(strip $(1))" ; exit 1)
endef

define vltr_lib_make_recipe
    @echo "[INF] Running make in $(VLTR_WRK_DIR)..."
    cd $(VLTR_WRK_DIR) && $(MAKE) -f V$(VLTR_TOP_SEL).mk default $(VLTR_OBJS)
    $(call vltr_lib_make_check_recipe, $(VLTR_TARG))
    $(foreach OBJ,$(VLTR_OBJS),$(call vltr_lib_make_check_recipe, $(VLTR_WRK_DIR)/$(OBJ))$(MAKE_NL))
    @echo "[INF] Archive okay."
endef

define vltr_lib_recipe
    @ $(strip $(call print_separator_bash,begin vltr_lib_recipe))
    $(vltr_chk_src_cfg_recipe)
    $(vltr_chk_src_list_recipe)
    $(vltr_chk_top_name_cfg_recipe)
    $(vltr_chk_top_name_recipe)
    $(vltr_chk_target_path_recipe)
    $(vltr_lib_create_dir_recipe)
    $(vltr_lib_verilate_recipe)
    $(vltr_lib_make_recipe)
    @ $(strip $(call print_separator_bash,end vltr_lib_recipe))
endef

define vltr_lib_hints
    $(eval $(call hints_targ , verilate       , Create testbench RTL library archive      ))
    $(eval $(call hints_targ , verilate-clean , Eliminate testbench RTL library working directory ))
endef

define vltr_lib_rules
    $(eval $(VLTR_TARG)    : $(VLTR_REQS)    ; $$(vltr_lib_recipe)     )
    $(eval verilate        : $(VLTR_TARG)    ; $(NULL)                 )
    $(eval verilate-clean  : $(NULL)         ; rm -rf $(VLTR_WRK_DIR)  )
endef

INCLUDED_VERILATOR := $(TRUE)
