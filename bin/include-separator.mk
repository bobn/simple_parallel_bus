# 
#  Copyright 2022 - 2023 Robert Newgard
# 
#  This file is part of the simple_parallel_bus project
# 
#  simple_parallel_bus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  simple_parallel_bus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
# 

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

# print_separator_bash <message>
define print_separator_bash
    PS_PARM=$$(date "+$1 %Y-%m-%d-%H:%M:%S");
    PS_COLS=$$($(call exe_chk, tput) cols);
    PS_COLS=$$(( $$PS_COLS - $${#PS_PARM} - 4 ));
    echo -n "== $$PS_PARM ";
    while [ $$PS_COLS -gt 0 ] ; do PS_COLS=$$(( $$PS_COLS - 1 )) ; echo -n "=" ; done;
    echo;
endef

INCLUDED_SEPARATOR := $(TRUE)



