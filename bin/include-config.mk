# 
#  Copyright 2022 - 2023 Robert Newgard
# 
#  This file is part of the simple_parallel_bus project
# 
#  simple_parallel_bus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  simple_parallel_bus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
# 

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

# get_list  <path> - returns contents of existing file at <path>; otherwise returns ""
# get_str   <path> - returns first string from existing file at <path>; otherwise returns ""
# get_bool  <path> - returns "1" if existing file at <path> contains "1"; otherwise returns ""
# cast_bool <txt>  - returns "1" if txt is "1"; otherwise returns ""
get_list  = $(strip $(file < $(strip $(1))))
get_str   = $(firstword $(call get_list,$(1)))
get_bool  = $(filter 1,$(call get_str,$(1)))
cast_bool = $(filter 1,$(1))

INCLUDED_CONFIG := $(TRUE)
