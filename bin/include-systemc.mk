#
# Copyright 2022 - 2023 Robert Newgard
#
# This file is part of the simple_parallel_bus project
#
# simple_parallel_bus is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# simple_parallel_bus is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
#

ifneq ($(INCLUDED_SYSTEMC),)
    $(warning systemc included earlier)
endif

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(INCLUDED_HINTS),)
    include bin/include-hints.mk
endif

ifeq ($(INCLUDED_SEPARATOR),)
    include bin/include-separator.mk
endif

ifeq ($(INCLUDED_CONFIG),)
    include bin/include-config.mk
endif

ifeq ($(INCLUDED_VERILATOR),)
    include bin/include-verilator.mk
endif

ifeq ($(SYSC_CFG),)
    SYSC_CFG := $(CFG)/Install/SystemC
endif

ifeq ($(SYSC_GXX),)
    SYSC_GXX := $(call exe_chk, g++)
endif

ifeq ($(SYSC_CFLAGS),)
    SYSC_CFLAGS := $(NULL)
endif

ifeq ($(SYSC_DFLAGS),)
    SYSC_DFLAGS := $(NULL)
endif


# -- Integrating SystemC -------------------------------------------------------
#
# 1. Configuration files that must exist (empty okay)
#
#     | File                                   | Description
#     |----------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/lib_hdr      | Library header files, list of X.h file names
#     | cfg/Testbench/$BENCH_NAME/lib_obj      | Library object files, list of X.a file names
#     | cfg/Testbench/$BENCH_NAME/link_libs    | Linker -l components, libX format, converted to -lX in make
#     | cfg/Testbench/$BENCH_NAME/tb_csrc      | Testbench C++ code
#     | cfg/Testbench/$BENCH_NAME/tb_hsrc      | Testbench C++ headers
#
#
# 2. Configuration files that may exist
#
#     | File                                           | Description
#     |------------------------------------------------|--------------------------------
#     | cfg/Testbench/$BENCH_NAME/Defines/PlusArgs/*   | File names used as plusarg keys, containing value strings
#     | cfg/Testbench/$BENCH_NAME/Defines/Parameters/* | File names used as plusarg and parameter keys, containing value strings
#
#     Notes
#         1. PlusArgs and Parameters are passed into the SystemC testbench using
#            "+<name>=<value>" arguments; these values are accessed in SystemC
#            by calling the Verilated::commandArgsPlusMatch() function
#
#
# 3. Variables that must be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SIM_CFG_DIR        | 
#     | SIM_WRK_DIR        | 
#     | SIM_TEST_CFG       | 
#     | SIM_TEST_SEL       | 
#
#
# 4. Variables that must be provided by include-verilator.mk
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | VLTR_INC           | 
#     | VLTR_INC_STD       |
#     | VLTR_OBJS          | 
#     | VLTR_TARG          | 
#     | VLTR_COV_REQ       |
#
#
# 5. Variables that may be provided by Makefile
#
#     | Variable           | Description
#     |--------------------|----------------------------------------------------
#     | SYSC_CFG           | 
#     | SYSC_GXX           | 
#     | SYSC_CFLAGS        | 
#     | SYSC_DFLAGS        | 
#
#
# 6. Add hints to Makefile
#     * Add the following to the '$(eval $(call hints_targ, ...' sequence
#
#         $(sysc_build_hints)
#         $(sysc_run_hints)
#
#
# 7. Add rules to Makefile
#     * Add the following to the rules sequence
#
#         $(sysc_build_rules)
#         $(sysc_run_rules)
#
#--


SYSC_VERS_CFG    := $(SYSC_CFG)/version
SYSC_VERS_DFLT   := 2.3.3
SYSC_VERS_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SYSC_VERS_CFG),$(SYSC_VERS_DFLT))))
SYSC_HOME_CFG    := $(SYSC_CFG)/home
SYSC_HOME_DFLT   := /opt/systemc
SYSC_HOME_SEL    := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SYSC_HOME_CFG),$(SYSC_HOME_DFLT))))
SYSC_INC_DIR     := $(SYSC_HOME_SEL)/$(SYSC_VERS_SEL)/include
SYSC_LIB_DIR     := $(SYSC_HOME_SEL)/$(SYSC_VERS_SEL)/lib-linux64
SYSC_LIB_ARCHIVE := $(SYSC_LIB_DIR)/libsystemc.a

SYSC_CFLAGS      := $(NULL)
SYSC_DFLAGS      := $(NULL)


#-- Build ----------------------------------------------------------------------
# build_cxx_obj_recipe <csrc>
define build_cxx_obj_recipe
    $(SYSC_GXX) -c $(SYSC_CFLAGS) $(SYSC_DFLAGS) -o $(VLTR_WRK_DIR)/$(basename $(notdir $(1))).o $(SYSC_BLD_IDIR) $(1)
endef

define sysc_build_cxx_all_recipe
    @echo "[INF] Compiling testbench C++ source..."
    $(foreach FIL,$(SYSC_CFG_CSRC_LST),$(call build_cxx_obj_recipe,$(FIL))$(MAKE_NL))
    @echo "[INF] Compile done."
endef

define sysc_build_link_recipe
    @echo "[INF] Link objects ..."
    $(SYSC_GXX) -o $(SYSC_BLD_EXE) $(SYSC_BLD_OBJS) $(SYSC_BLD_LOPT)
    @echo "[INF] Link done."
endef

define sysc_build_recipe
    @ $(strip $(call print_separator_bash,begin sysc_build_recipe))
    $(sysc_build_cxx_all_recipe)
    $(sysc_build_link_recipe)
    @ $(strip $(call print_separator_bash,end sysc_build_recipe))
endef

define sysc_build_hints
    $(eval $(call hints_targ , build , Build testbench executable ))
endef

define sysc_build_rules
    $(eval $(SYSC_BLD_TARG) : $(SYSC_BLD_REQS) ; $$(sysc_build_recipe) )
    $(eval build            : $(SYSC_BLD_TARG) ; $(NULL)               )
endef


#-- Run ------------------------------------------------------------------------
define sysc_run_recipe
    @ $(strip $(call print_separator_bash,begin sysc_run_recipe))
    set -o pipefail && $(SYSC_BLD_EXE) $(SIM_TEST_SEL) $(SYSC_RUN_VCD) |& tee $(SYSC_RUN_LOG)
    @ $(strip $(call print_separator_bash,end sysc_run_recipe))
endef

define sysc_rerun_recipe
    rm -rf $(SYSC_RUN_VCD)
    $(sysc_run_recipe)
endef

define sysc_run_hints
    $(eval $(call hints_targ , run   , Run testbench executable    ))
    $(eval $(call hints_targ , rerun , Re-run testbench executable ))
endef

define sysc_run_rules
    $(eval $(SYSC_RUN_TARG) : $(SYSC_RUN_REQS) ; $$(sysc_run_recipe)   )
    $(eval run              : $(SYSC_RUN_TARG) ; $(NULL)               )
    $(eval rerun            : $(SYSC_RUN_TARG) ; $$(sysc_rerun_recipe) )
endef

export SYSTEMC_INCLUDE := $(SYSC_INC_DIR)
export SYSTEMC_LIBDIR  := $(SYSC_LIB_DIR)

$(call make_pkg_append_var, SC_COPYRIGHT_MESSAGE, DISABLE)

INCLUDED_SYSTEMC := $(TRUE)
