# 
#  Copyright 2022 - 2023 Robert Newgard
# 
#  This file is part of the simple_parallel_bus project
# 
#  simple_parallel_bus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  simple_parallel_bus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
# 

ifeq ($(INCLUDED_BASE),)
    include bin/include-base.mk
endif

ifeq ($(HINTS_TF),)
    undefine HINTS_TF
    HINTS_TF := %-20s
endif

HINTS  := hints-hdr
PHONYS += hints-hdr hints

# hints_def <target> <description>
hints_def = printf "$(HINTS_TF) %s\n" "$(strip $(1))" "$(strip $(2))" ;

define hints_header_recipe
    @ $(call hints_def,target,description)
    @ $(call hints_def,------------,--------------------------------------------------------)
endef

# hints_targ <target> <description>
define hints_targ
    $(strip $(1))-hint : $(NULL) ; @ $(call hints_def , $(strip $(1)) , $(strip $(2)) )

    HINTS  += $(strip $(1))-hint
    PHONYS += $(strip $(1))-hint
    PHONYS += $(strip $(1))
endef

define hints_rules
    $(eval hints-hdr : $(NULL)  ; $$(hints_header_recipe) )
    $(eval hints     : $(HINTS) ; $(NULL)                 )
endef

INCLUDED_HINTS := $(TRUE)
