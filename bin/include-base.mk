# 
#  Copyright 2022 - 2023 Robert Newgard
# 
#  This file is part of the simple_parallel_bus project
# 
#  simple_parallel_bus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  simple_parallel_bus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
# 

undefine FALSE
undefine NULL

SHELL    := /usr/bin/bash
SH_VARS  := $(NULL)
PHONYS   := $(NULL)
BIN      := bin
CFG      := cfg
CWD      := $(abspath $(CURDIR))
TMP      := tmp
TRUE     := true
MAKE_PKG := $(CWD)/$(TMP)/make_pkg.bash

$(if $(filter shell-export,$(.FEATURES)),$(NULL),$(error Make does not support the shell-export feature))

define make_newline_def
$(NULL)
$(NULL)
endef

MAKE_NL := $(make_newline_def)
SP      := $(shell printf "\x20")
FS      := /

exe_chk = $(if $(shell type -p $(strip $(1))),$(strip $(1)),$(error Executable for "$(strip $(1))" not found))

XARGS := $(call exe_chk, xargs)

# make_pkg_cfg_dflt_bash <path> <default>
#
#     * Emits a value
#     * If the file <path> exists
#         * Value is the contents of <path> if it exists
#     * Otherwise
#         * The file <path> is created
#         * The contents of <path> is <default>
#         * Value is <default>
#
define make_pkg_cfg_dflt_bash
    CFG_PATH=$(strip $(1)) ;
    if [ -z "$$CFG_PATH" ] ; then
        echo "[ERR] make_pkg_cfg_dflt_bash() CFG_PATH is empty.  Stop." > /dev/stderr ;
        exit 1;
    fi;
    CFG_DFLT="$(strip $(2))" ;
    if [ -z "$$CFG_DFLT" ] ; then
        echo "[ERR] make_pkg_cfg_dflt_bash() CFG_DFLT is empty.  Stop." > /dev/stderr ;
        exit 1;
    fi;
    CFG_DIR=$$(dirname $$CFG_PATH) ;
    if [ ! -d "$$CFG_DIR" ] ; then
        mkdir -p $$CFG_DIR ;
    fi ;
    if [ ! -r "$$CFG_PATH" ] ; then
        echo "$$CFG_DFLT" | $(XARGS) > $$CFG_PATH ;
    fi ;
    cat $$CFG_PATH | $(XARGS) ;
endef

# make_pkg_export_var <var> <val>
#
#     Export a variable <var> to the shell or sub-make with value <var>
#
define make_pkg_export_var
    $(if $(filter $(strip $(1)),$(SH_VARS)),$(NULL),SH_VARS += $(strip $(1)))
    export $(strip $(1)) $(if $(filter $(strip $(1)),$(SH_VARS)),+=,:=) $(strip $(2))
endef

# make_pkg_export_prepath <var> <val>
#
#     Export a variable <var> to the shell or sub-make with value <var>=<val>:<var>
#
define make_pkg_export_prepath
    $(if $(filter $(strip $(1)),$(SH_VARS)),$(NULL),SH_VARS += $(strip $(1)))
    export $(strip $(1)) := $(strip $(2)):$($(strip $(1)))
endef

$(shell test ! -e $(TMP)     && (set -x ; mkdir -p $(TMP)))

$(eval $(call make_pkg_export_var,     PRJ_CWD, $(CWD)         ))
$(eval $(call make_pkg_export_var,     PRJ_BIN, $(BIN)         ))
$(eval $(call make_pkg_export_var,     PRJ_CFG, $(CFG)         ))
$(eval $(call make_pkg_export_var,     PRJ_TMP, $(TMP)         ))
$(eval $(call make_pkg_export_prepath, PATH,    $(CWD)/$(BIN)  ))

INCLUDED_BASE := $(TRUE)
