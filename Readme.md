# First Steps
TBD


# Make Target Overview
Typing 'make' on the command line will return a list of
make targets and hints about what they are used for

# Project Sub-Directories
| Variable  | Value     | Description                                          |
|-----------|-----------|------------------------------------------------------|
| BIN       | bin       | Contains scripts used for sim and validation         |
| CFG       | cfg       | Contains the project configuration database          |
| RTL       | rtl       | Contains SystemVerilog RTL                           |
| SIM       | sim       | Contains SystemVerilog code for the testbench        |
| TMP       | tmp       | Contains subdirectories and files created under sim  |

## Reserved Subdirectory Names

For various reasons, the list of reserved subdirectory names is

* Install

no source-code subdirectories may have one of these names.

# Project Variables
The variables specified here are used in the following descriptions.

| Variable  | Description                                         |
|-----------|-----------------------------------------------------|
| REPO      | Git repository name                                 |
| TOOL      | Tool tag                                            |
| TB_TOP    | Selected testbench                                  |
| TB_TEST   | Selected test                                       |

# Project Configuration Database (a.k.a Registry):
The database is used to configure how testbenches are built and run.  It
resides within the $CFG directory.  The database is read every time that
`make` is run.

To view the entire configuration database, use the make target
'show-cfg'.  Other make targets (show-cfg-install,
show-cfg-testbench) show portions of the configuration database.


## Database elements
* Subdirectories
    * Major subdirectories
        * Install
        * Testbench
    * Install subdirectories
        * Install subdirectory names are tags for installed tools
    * Testbench subdirectories
        * Testbench subdirectory names specify testbench components
* Files
    * Files may contain Booleans, Strings or Lists
    * Files are interpreted using UNIX file semantics
        * Make may incorrectly parse a configuration file if it has a
          DOS filetype or has been edited in a Windows environment
        * Check that file lines end with ASCII LF
        * Check that files do not contain ASCII FF or CR characters
    * Whitespace characters, a term used below, are the ASCII characters
      SPACE, HT, and LF
* File contents
    * Boolean values
        * A file containing a string made up of a single "1" or "0" character
    * String values
        * A file containing a single sequence of non-whitespace characters
    * List values
        * A file containing multiple strings bounded by whitespace characters

## Version control
Database element files may be controlled by Git or may be created
dynamically

* Those controlled by Git
    * will appear upon clone, as usual
    * when modified, may be retained and shared when the file is part of a Git
      commit
* Those dynamically created
    * will take a default value upon the first invocation of `make` within
      a clone
    * will, when modified, retain the modified value until modified again


## Changing configuration entries
* Changes can be made to the configuration by modifying the files containing
  boolean, string, or list values
* A configuration entry file can be changed in a text editor, but keep in mind
  the UNIX-only file semantics
* Alternatively, a configuration entry file can be changed by shell redirect
    <pre>
    echo "value" > file
    </pre>


## Configuration entries
### Boolean entries
* In $CFG/Testbench
    * TBD

### String entries
* In $CFG/Testbench
    * active_testbench
        * Specifies a testbench to build and run
        * Dynamically created, defaults to "tb_lint"
    * $TB_TOP/active_test
        * Specifies a test within $TB_TOP to execute
        * Dynamically created, defaults to "default"
    * $TB_TOP/tb_vsrc_top
        * Specifies top-level testbench module
        * Dynamically created, defaults to "default"
* In $CFG/Install
    * $TOOL/home
        * Specifies a filesystem path to the install directory
          for $TOOL
        * Dynamically created, defaults depend on tool
    * $TOOL/version
        * Specifies the version of the $TOOL
        * Dynamically created, defaults depend on tool
* $CFG/magic
    * Unique string used to validate the $CFG directory
    * This file is under Git control

### List entries
* In $CFG/Testbench
    * $TB_TOP/tb_vsrc
        * Specifies RTL components
        * This file is under Git control

# Licenses

## License for Code

The code in this project is licensed under the GPLv3

## License for This Project Summary

This work is licensed under the Creative Commons Attribution-ShareAlike 3.0
Unported License. To view a copy of this license, visit
http://creativecommons.org/licenses/by-sa/3.0/. 

<style>
    body
    {
        font-family: serif;
        counter-reset: cnth1;
    }
    h1:before
    {
        content: counter(cnth1) ". ";
        counter-increment: cnth1;
    }
    h1
    {
        font-size:     1em;
        counter-reset: cnth2;
    }
    h2:before
    {
        content: counter(cnth1) "."
                 counter(cnth2) ". ";
        counter-increment: cnth2;
    }
    h2
    {
        font-size:     1em;
        counter-reset: cnth3;
    }
    h3:before
    {
        content: counter(cnth1) "."
                 counter(cnth2) "."
                 counter(cnth3) ". ";
        counter-increment: cnth3;
    }
    h3
    {
        font-size:     1em;
        counter-reset: cnth4;
    }
    h4:before
    {
        content: counter(cnth1) "."
                 counter(cnth2) "."
                 counter(cnth3) "."
                 counter(cnth4) ". ";
      counter-increment: cnth4;
    }
    h4
    {
        font-size:     1em;
        counter-reset: cnth5;
    }
    h5:before
    {
        content: counter(cnth1) "."
                 counter(cnth2) "."
                 counter(cnth3) "."
                 counter(cnth4) "."
                 counter(cnth5) ". ";
      counter-increment: cnth5;
    }
    h5
    {
        font-size:     1em;
        counter-reset: cnth6;
    }
</style>
