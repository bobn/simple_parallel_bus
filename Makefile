# 
#  Copyright 2022 - 2023 Robert Newgard
# 
#  This file is part of the simple_parallel_bus project
# 
#  simple_parallel_bus is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
# 
#  simple_parallel_bus is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
# 

# -- Make Base Package ---------------------------------------------------------
include bin/include-base.mk


# -- Make Environment ----------------------------------------------------------
DISCOUNT  := $(call exe_chk, markdown)
HOSTNAME  := $(call exe_chk, hostname)
SED       := $(call exe_chk, sed)
TREE      := $(call exe_chk, tree)
VIRT_CK   := $(call exe_chk, systemd-detect-virt)

HINTS_TF  := %-24s
ENV_TF    := %-24s
PPR4      := pr -t -o 4

include bin/include-hints.mk
include bin/include-separator.mk
include bin/include-config.mk

define show_tree_bash
    IGN="" ;
    set -f ;
    for PAT in $$(cat .gitignore) ; do
        ITM=$$(echo $$PAT | $(SED) 's|/||') ;
        if [ -z $$IGN ] ; then
            IGN=$$ITM ;
        else
            IGN=$$(printf "%s|%s" $$IGN $$ITM) ;
        fi ;
    done ;
    set +f ;
    $(TREE) --noreport -fCI "$$IGN" . ;
endef



# -- Project Environment -------------------------------------------------------
RTL  := rtl

CFG_SHOW_EXE     := $(call exe_chk, cfg-show)
GIT_REPO_EXE     := $(call exe_chk, git-repo)

define show_env_bash
    printf "\n" ;
    printf "$(ENV_TF) %s\n" "Host Name:"            "$$($(HOSTNAME))" ;
    printf "$(ENV_TF) %s\n" "Virtualization:"       "$$($(VIRT_CK))" ;
    printf "$(ENV_TF) %s\n" "bash Version:"         "$$(echo $$BASH_VERSION)" ;
    printf "$(ENV_TF) %s\n" "make Version:"         "$(MAKE_VERSION)" ;
    printf "$(ENV_TF) %s\n" "Verilator Version:"    "$(VLTR_VERS_SEL)" ;
    printf "$(ENV_TF) %s\n" "Project Clone URL:"    "$$($(GIT_REPO_EXE) . url)" ;
    printf "$(ENV_TF) %s\n" "Project Clone Branch:" "$$($(GIT_REPO_EXE) . branch)" ;
    printf "\n" ;
endef

include $(BIN)/include-verilator.mk
include $(BIN)/include-systemc.mk



# -- Simulation -----------------------------------------------------------------
SIM_TB_CFG        := $(CFG)/Testbench/active_testbench
SIM_TB_DFLT       := tb_lint
SIM_TB_SEL        := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SIM_TB_CFG),$(SIM_TB_DFLT))))
SIM_CFG_DIR       := $(CFG)/Testbench/$(SIM_TB_SEL)
SIM_TEST_CFG      := $(SIM_CFG_DIR)/active_test
SIM_TEST_DFLT     := debug
SIM_TEST_SEL      := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(SIM_TEST_CFG),$(SIM_TEST_DFLT))))



# -- Verilated RTL --------------------------------------------------------------
VLTR_TOP_CFG      := $(SIM_CFG_DIR)/tb_vsrc_top
VLTR_TOP_DFLT     := tb_lint_top
VLTR_TOP_SEL      := $(shell $(strip $(call make_pkg_cfg_dflt_bash,$(VLTR_TOP_CFG),$(VLTR_TOP_DFLT))))
VLTR_SRC_CFG      := $(SIM_CFG_DIR)/tb_vsrc
VLTR_SRC_LST      := $(call get_list, $(VLTR_SRC_CFG))
VLTR_WRK_DIR      := $(TMP)/sysc/$(SIM_TB_SEL)/vltd
VLTR_REQS         := $(VLTR_SRC_LST) $(VLTR_SRC_CFG) $(VLTR_TOP_CFG) $(SIM_TB_CFG)
VLTR_TARG         := $(VLTR_WRK_DIR)/V$(VLTR_TOP_SEL)__ALL.a



# -- Lint ----------------------------------------------------------------------
LINT_RTL_FILES  := $(VLTR_SRC_LST)
LINT_NOWRN      := -Wno-multitop
LINT            := $(VERILATOR) $(LINT_NOWRN) --lint-only 



# -- Eval Package --------------------------------------------------------------
include bin/include-eval.mk



# -- Hints ----------------------------------------------------------------------
$(eval $(call hints_targ , show-cfg           , Show project config - aka 'sc'                          ))
$(eval $(call hints_targ , show-cfg-install   , Show project install config - aka 'sci'                 ))
$(eval $(call hints_targ , show-cfg-testbench , Show project install config - aka 'sct'                 ))
$(eval $(call hints_targ , show-env           , Show simulation environment - aka 'se'                  ))
$(eval $(call hints_targ , show-tree          , Show managed files in tree - aka 'st'                   ))
$(eval $(call hints_targ , readme             , Create Readme.html from Readme.md                       ))
$(eval $(call hints_targ , lint               , Check generated verilog packages                        ))
$(vltr_lib_hints)


# -- rules ---------------------------------------------------------------------
nil                    : $(NULL)               ; @true
dbg-%                  : $(NULL)               ; @echo '$* = "$(strip $($*))"'
show-cfg               : $(NULL)               ; @ $(CFG_SHOW_EXE)
sc                     : show-cfg              ; $(NULL)
show-cfg-install       : $(NULL)               ; @ $(CFG_SHOW_EXE) $(CFG)/Install
sci                    : show-cfg-install      ; $(NULL)
show-cfg-testbench     : $(NULL)               ; @ $(CFG_SHOW_EXE) $(CFG)/Testbench/$(SIM_TB_SEL)
sct                    : show-cfg-testbench    ; $(NULL)
show-env               : $(NULL)               ; @ ($(strip $(show_env_bash))) | $(PPR4)
se                     : show-env              ; $(NULL)
show-tree              : $(NULL)               ; @ $(strip $(show_tree_bash))
st                     : show-tree             ; $(NULL)
Readme.html            : Readme.md             ; @ cat Readme.md | $(DISCOUNT) -f toc -T -S > Readme.html
readme                 : Readme.html           ; $(NULL)
lint                   : $(NULL)               ; $(LINT) $(LINT_RTL_FILES)

$(hints_rules)
$(vltr_lib_rules)

.PHONY : nil sc sci se st $(PHONYS)
.DEFAULT_GOAL := hints
