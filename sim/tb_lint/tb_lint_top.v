/*
 *  Copyright 2022 - 2023 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

module tb_lint_top import spb_pkg::*, tb_lint_top_pkg::*;
    (
        output t_req [SMSB:0] req_dn_o,
        input  t_ack [SMSB:0] ack_dn_i,
        input  t_req          req_up_i,
        output t_ack          ack_up_o,
        input                 clk_i,
        input                 rst_i
    );

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    import spb_pkg::*;
    import tb_lint_top_pkg::*;

    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Interface Instance ---------------------------------------------------
    spb_ifc spb_req_up();
    spb_ifc spb_ack_up();
    spb_ifc spb_req_dn[SMSB:0]();
    spb_ifc spb_ack_dn[SMSB:0]();

    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    for (genvar i = 0 ; i < SIZE ; i++) begin : g_ack_dn
        assign spb_ack_dn[i].ack  = ack_dn_i[i].ack;
        assign spb_ack_dn[i].nok  = ack_dn_i[i].nok;
        assign spb_ack_dn[i].irq  = ack_dn_i[i].irq;
        assign spb_ack_dn[i].data = ack_dn_i[i].data;
    end

    assign spb_req_up.req  = req_up_i.req ;
    assign spb_req_up.wren = req_up_i.wren;
    assign spb_req_up.addr = req_up_i.addr;
    assign spb_req_up.data = req_up_i.data;

    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------

    //- Module Instance ------------------------------------------------------
    spb_bridge
    #(
        .ABITS ( 32   ),
        .DBITS ( 32   ),
        .SIZE  ( SIZE ),
        .SPEC  ( SPEC )
    )
    i_br
    (
        .req_up_i ( spb_req_up         ),
        .ack_up_o ( spb_ack_up         ),
        .req_dn_o ( spb_req_dn[SMSB:0] ),
        .ack_dn_i ( spb_ack_dn[SMSB:0] ),
        .clk_i    ( clk_i              ),
        .rst_i    ( rst_i              )
    );

    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    assign ack_up_o.ack  = spb_ack_up.ack;
    assign ack_up_o.nok  = spb_ack_up.nok;
    assign ack_up_o.irq  = spb_ack_up.irq;
    assign ack_up_o.data = spb_ack_up.data;

    for (genvar i = 0 ; i < SIZE ; i++) begin : g_req_dn
        assign req_dn_o[i].req  = spb_req_dn[i].req;
        assign req_dn_o[i].wren = spb_req_dn[i].wren;
        assign req_dn_o[i].addr = spb_req_dn[i].addr;
        assign req_dn_o[i].data = spb_req_dn[i].data;
    end

    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------
    if (SIZE < 1) begin
        $fatal("[ERR] SIZE < 1");
    end else begin
        $info("[INF] SIZE is %0d", SIZE);
    end

endmodule
