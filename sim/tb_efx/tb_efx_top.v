/*
 *  Copyright 2022 - 2023 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

module tb_efx_top import spb_pkg::*, tb_efx_top_pkg::*;
    (
        output       req_dn_2_req_o,
        output       req_dn_2_wren_o,
        output [7:0] req_dn_2_addr_o,
        output [7:0] req_dn_2_data_o,
        output       req_dn_1_req_o,
        output       req_dn_1_wren_o,
        output [7:0] req_dn_1_addr_o,
        output [7:0] req_dn_1_data_o,
        output       req_dn_0_req_o,
        output       req_dn_0_wren_o,
        output [7:0] req_dn_0_addr_o,
        output [7:0] req_dn_0_data_o,
        input        ack_dn_2_ack_i,
        input        ack_dn_2_nok_i,
        input        ack_dn_2_irq_i,
        input  [7:0] ack_dn_2_data_i,
        input        ack_dn_1_ack_i,
        input        ack_dn_1_nok_i,
        input        ack_dn_1_irq_i,
        input  [7:0] ack_dn_1_data_i,
        input        ack_dn_0_ack_i,
        input        ack_dn_0_nok_i,
        input        ack_dn_0_irq_i,
        input  [7:0] ack_dn_0_data_i,
        output       ack_up_ack_o,
        output       ack_up_nok_o,
        output       ack_up_irq_o,
        output [7:0] ack_up_data_o,
        input        req_up_req_i,
        input        req_up_wren_i,
        input  [7:0] req_up_addr_i,
        input  [7:0] req_up_data_i,
        input        clk_i,
        input        rst_i
    );

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    import spb_pkg::*;
    import tb_efx_top_pkg::*;

    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Interface Instance ---------------------------------------------------
    spb_ifc spb_req_up();
    spb_ifc spb_ack_up();
    spb_ifc spb_req_dn[SMSB:0]();
    spb_ifc spb_ack_dn[SMSB:0]();

    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    assign spb_ack_dn[2].ack        = ack_dn_2_ack_i;
    assign spb_ack_dn[2].nok        = ack_dn_2_nok_i;
    assign spb_ack_dn[2].irq        = ack_dn_2_irq_i;
    assign spb_ack_dn[2].data[63:8] = '0;
    assign spb_ack_dn[2].data[7:0]  = ack_dn_2_data_i[7:0];

    assign spb_ack_dn[1].ack        = ack_dn_1_ack_i;
    assign spb_ack_dn[1].nok        = ack_dn_1_nok_i;
    assign spb_ack_dn[1].irq        = ack_dn_1_irq_i;
    assign spb_ack_dn[1].data[63:8] = '0;
    assign spb_ack_dn[1].data[7:0]  = ack_dn_1_data_i[7:0];

    assign spb_ack_dn[0].ack        = ack_dn_0_ack_i;
    assign spb_ack_dn[0].nok        = ack_dn_0_nok_i;
    assign spb_ack_dn[0].irq        = ack_dn_0_irq_i;
    assign spb_ack_dn[0].data[63:8] = '0;
    assign spb_ack_dn[0].data[7:0]  = ack_dn_0_data_i[7:0];

    assign spb_req_up.req           = req_up_req_i;
    assign spb_req_up.wren          = req_up_wren_i;
    assign spb_req_up.addr[63:8]    = '0;
    assign spb_req_up.addr[7:0]     = req_up_addr_i[7:0];
    assign spb_req_up.data[63:8]    = '0;
    assign spb_req_up.data[7:0]     = req_up_data_i[7:0];

    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------

    //- Module Instance ------------------------------------------------------
    spb_bridge
    #(
        .ABITS ( 32   ),
        .DBITS ( 32   ),
        .SIZE  ( SIZE ),
        .SPEC  ( SPEC )
    )
    i_br
    (
        .req_up_i ( spb_req_up         ),
        .ack_up_o ( spb_ack_up         ),
        .req_dn_o ( spb_req_dn[SMSB:0] ),
        .ack_dn_i ( spb_ack_dn[SMSB:0] ),
        .clk_i    ( clk_i              ),
        .rst_i    ( rst_i              )
    );

    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    assign ack_up_ack_o         = spb_ack_up.ack;
    assign ack_up_nok_o         = spb_ack_up.nok;
    assign ack_up_irq_o         = spb_ack_up.irq;
    assign ack_up_data_o[7:0]   = spb_ack_up.data[7:0];

    assign req_dn_2_req_o       = spb_req_dn[2].req;
    assign req_dn_2_wren_o      = spb_req_dn[2].wren;
    assign req_dn_2_addr_o[7:0] = spb_req_dn[2].addr[7:0];
    assign req_dn_2_data_o[7:0] = spb_req_dn[2].data[7:0];

    assign req_dn_1_req_o       = spb_req_dn[1].req;
    assign req_dn_1_wren_o      = spb_req_dn[1].wren;
    assign req_dn_1_addr_o[7:0] = spb_req_dn[1].addr[7:0];
    assign req_dn_1_data_o[7:0] = spb_req_dn[1].data[7:0];

    assign req_dn_0_req_o       = spb_req_dn[0].req;
    assign req_dn_0_wren_o      = spb_req_dn[0].wren;
    assign req_dn_0_addr_o[7:0] = spb_req_dn[0].addr[7:0];
    assign req_dn_0_data_o[7:0] = spb_req_dn[0].data[7:0];

    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------
    if (SIZE < 1) begin
        $fatal("[ERR] SIZE < 1");
    end else begin
        $info("[INF] SIZE is %0d", SIZE);
    end

endmodule
