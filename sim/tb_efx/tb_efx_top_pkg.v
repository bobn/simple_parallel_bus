/*
 *  Copyright 2022 - 2023 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

package tb_efx_top_pkg;

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    import spb_pkg::*;

    //- Type and Parameter ---------------------------------------------------
    localparam t_decode [2:0] SPEC = {
        { 32'h00000018, 32'h000000ff, 32'hffffffff },
        { 32'h0000000c, 32'h00000017, 32'hffffffff },
        { 32'h00000000, 32'h0000000b, 32'hffffffff }
    };

    localparam integer unsigned  SIZE = $size(SPEC);
    localparam integer unsigned  SMSB = (SIZE - 1);

    //- Task and Function : pure ---------------------------------------------
    //- Class Definition: pure -----------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------

endpackage
