/*
 *  Copyright 2022 - 2023 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

package spb_pkg;
    parameter integer unsigned ABITS = 32;
    parameter integer unsigned DBITS = 32;
    parameter integer unsigned AMSB  = (ABITS - 1);
    parameter integer unsigned DMSB  = (DBITS - 1);

    typedef struct packed
    {
        logic [AMSB:0] amin;
        logic [AMSB:0] amax;
        logic [AMSB:0] amsk;
    }
    t_decode;

    localparam t_decode DECODE_NIL = '0;

endpackage
