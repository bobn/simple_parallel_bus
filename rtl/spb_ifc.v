/*
 *  Copyright 2022 - 2023 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

interface spb_ifc;
    wire        req;
    wire        ack;
    wire        nok;
    wire        wren;
    wire        irq;
    wire [63:0] data;
    wire [63:0] addr;

    modport req_up
    (
        input  req,
        input  wren,
        input  addr,
        input  data
    );

    modport req_dn
    (
        output req,
        output wren,
        output addr,
        output data
    );

    modport ack_up
    (
        output ack,
        output nok,
        output irq,
        output data
    );

    modport ack_dn
    (
        input  ack,
        input  nok,
        input  irq,
        input  data
    );

endinterface
