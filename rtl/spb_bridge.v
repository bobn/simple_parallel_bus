/*
 *  Copyright 2022 - 2024 Robert Newgard
 *
 *  This file is part of the simple_parallel_bus project
 *
 *  simple_parallel_bus is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  simple_parallel_bus is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with simple_parallel_bus.  If not, see <https://www.gnu.org/licenses/>.
 */

module spb_bridge import spb_pkg::*;
    #(
        parameter integer unsigned  ABITS = 32,
        parameter integer unsigned  DBITS = 32,
        parameter integer unsigned  SIZE  = 1,
        parameter integer unsigned  AMSB  = (ABITS - 1),
        parameter integer unsigned  DMSB  = (DBITS - 1),
        parameter integer unsigned  SMSB  = (SIZE - 1),
        parameter t_decode [SMSB:0] SPEC  = {SIZE{DECODE_NIL}},
        parameter integer unsigned  TMOUT = 20
    )
    (
        output          up_ack_o,
        output          up_nok_o,
        output          up_irq_o,
        output [63:0]   up_data_o,
        input           up_req_i,
        input           up_wren_i,
        input  [63:0]   up_addr_i,
        input  [63:0]   up_data_i,

        output [SMSB:0] dn_req_o,
        output          dn_wren_o,
        output [63:0]   dn_addr_o,
        output [63:0]   dn_data_o,
        input           dn_ack_i,
        input           dn_nok_i,
        input           dn_irq_i,
        input  [63:0]   dn_data_i,

        input           clk_i,
        input           rst_i
    );

    //- Time Spec ------------------------------------------------------------
    //- Import ---------------------------------------------------------------
    //- Type and Parameter ---------------------------------------------------
    typedef enum [1:0]
    {
        SPB_IDLE,
        SPB_ACTIVE,
        SPB_RECOV,
        SPB_ERR
    }
    t_state;

    //- Task and Function : pure ---------------------------------------------
    function automatic logic sel_dn(t_decode arg_spec, logic [63:0] arg_addr);
        logic          ret;
        logic [AMSB:0] addr;

        ret          = '0;
        addr[AMSB:0] = arg_addr[AMSB:0] & arg_spec.amsk[AMSB:0];

        if ((addr[AMSB:0] >= arg_spec.amin[AMSB:0]) && (addr[AMSB:0] <= arg_spec.amax[AMSB:0])) begin
            ret = '1;
        end

        return ret;
    endfunction

    //- Class Definition: pure -----------------------------------------------
    //- Signal and Pure Class Declaration ------------------------------------
    logic [SMSB:0] dn_req;
    logic          dn_wren;
    logic [63:0]   dn_addr;
    logic [63:0]   dn_data;

    logic          up_ack;
    logic          up_nok;
    logic          up_irq;
    logic [63:0]   up_data;

    integer        fsm_sel;
    integer        fsm_tmout;
    t_state        fsm_state;

    //- Input Assignment -----------------------------------------------------
    //- Task and Function: impure --------------------------------------------
    //- Class Definition: impure ---------------------------------------------
    //- Class Declaration: impure --------------------------------------------
    //- Interface Instance ---------------------------------------------------
    //- Module Instance ------------------------------------------------------
    //- RTL ------------------------------------------------------------------
    always_ff @(posedge clk_i) begin
        if (rst_i) begin
            dn_req[SMSB:0] <= '0;
            dn_wren        <= '0;
            dn_addr[63:0]  <= '0;
            dn_data[63:0]  <= '0;

            up_ack         <= '0;
            up_nok         <= '0;
            up_irq         <= '0;
            up_data[63:0]  <= '0;

            fsm_sel        <= '0;
            fsm_tmout      <= '0;
            fsm_state      <= SPB_IDLE;
        end else begin
            up_irq         <= dn_irq_i;
            up_ack         <= '0;
            up_nok         <= '0;

            case (fsm_state)
                SPB_IDLE : begin
                    dn_req[SMSB:0] <= '0;

                    if (up_req_i) begin
                        fsm_state     <= SPB_ERR;

                        for (integer i = 0 ; i < SIZE ; i++) begin
                            if (sel_dn(SPEC[i], up_addr_i[63:0])) begin
                                dn_req[i] <= '1;
                                fsm_sel   <= i;
                                fsm_tmout <= '0;
                                fsm_state <= SPB_ACTIVE;

                                break;
                            end
                        end
                    end
                end
                SPB_ACTIVE : begin
                    fsm_tmout <= fsm_tmout + 1;

                    if (fsm_tmout > TMOUT) begin
                        up_ack          <= '1;
                        up_nok          <= '1;
                        up_data[63:0]   <= '0;
                        dn_req[fsm_sel] <= '0;
                        fsm_state       <= SPB_RECOV;
                    end else if (dn_ack_i) begin
                        up_ack          <= '1;
                        dn_req[fsm_sel] <= '0;
                        fsm_state       <= SPB_RECOV;
                    end
                end
                SPB_RECOV : begin
                    if (!up_req_i) begin
                        fsm_state   <= SPB_IDLE;
                    end
                end
                SPB_ERR : begin
                    up_ack         <= '1;
                    up_nok         <= '1;
                    up_data [63:0] <= '0;
                    fsm_state      <= SPB_RECOV;
                end
            endcase
        end
    end

    //- Behavioral -----------------------------------------------------------
    //- Output Assignment ----------------------------------------------------
    assign dn_req_o[SMSB:0] = dn_req[SMSB:0];
    assign dn_wren_o        = dn_wren;
    assign dn_addr_o[63:0]  = dn_addr[63:0];
    assign dn_data_o[63:0]  = dn_data[63:0];

    assign up_ack_o         = up_ack;
    assign up_nok_o         = up_nok;
    assign up_irq_o         = up_irq;
    assign up_data_o[63:0]  = up_data[63:0];

    //- Assertion: runtime ---------------------------------------------------
    //- Assertion: elaboration -----------------------------------------------
    if (ABITS < 1) begin
        $fatal("[ERR] ABITS < 1");
    end
    if (DBITS < 8) begin
        $fatal("[ERR] DBITS < 8");
    end
    if ($countones(DBITS) != 1) begin
        $fatal("[ERR] DBITS is not an integer exponent of 2");
    end
    if (SIZE < 1) begin
        $fatal("[ERR] SIZE < 1");
    end
    for (genvar i = 0 ; i < SIZE ; i++) begin
        if (SPEC[i].amin > SPEC[i].amax) begin
            $fatal("[ERR] Minimum address is greater than maximum address in SPEC index %0d", i);
        end
    end

endmodule
